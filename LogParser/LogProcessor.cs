﻿using LogParser.Exceptions;
using LogParser.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
namespace LogParser
{
    public class LogProcessor : ILogProcessor
    {
        private readonly ILogParser _parser;
        private readonly ILogAnalyzer _analyzer;
        private readonly ILogger<LogProcessor> _logger;
        public LogProcessor(ILogParser parser, ILogAnalyzer analyzer, ILogger<LogProcessor> logger)
        {
            _logger = logger;
            _parser = parser;
            _analyzer = analyzer;
        }
        public LogProcessorResult ProcessLogsFromFile(string fileName)
        {
            try
            {
                var parseData = _parser.ParseLogFromFile(fileName);
                List<LogParserResult> logData = new List<LogParserResult>(parseData);
                int distinctIP = _analyzer.GetUniqueIpAddressCount(logData);
                var mostActiveIP = _analyzer.GetMostActiveIP(logData);
                var mostVisitedURL = _analyzer.GetMostVisitedUrl(logData);
                return new LogProcessorResult()
                {
                    MostActiveIPAddress = mostActiveIP,
                    UniqueIPCount = distinctIP,
                    MostVisitedUrl = mostVisitedURL
                };

            }
            catch(FileNotFoundException ex)
            {
                _logger.LogError($"{fileName} is not on the disk. {ex.Message} {ex.ToString()}");
            }
            return null;
        }
    }
}
