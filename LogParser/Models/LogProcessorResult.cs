﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Models
{
    public class LogProcessorResult
    {
        public int UniqueIPCount { get; set; }
        public IReadOnlyCollection<string> MostActiveIPAddress { get; set; }
        public IReadOnlyCollection<string> MostVisitedUrl { get; set; }
    }
}
