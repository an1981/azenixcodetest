﻿using LogParser.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser
{
    public interface ILogProcessor
    {
         LogProcessorResult ProcessLogsFromFile(string fileName); 
    }
}
