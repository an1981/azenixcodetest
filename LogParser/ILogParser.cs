﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser
{
    public interface ILogParser
    {
        IReadOnlyCollection<LogParserResult> ParseLogFromFile(string fileName);

    }
}
