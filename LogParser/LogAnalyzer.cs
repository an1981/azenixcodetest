﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogParser
{
    public class LogAnalyzer : ILogAnalyzer
    {
        public IReadOnlyCollection<string> GetMostActiveIP(ICollection<LogParserResult> data,int numberOfActiveIP=3)
        {
            var groupedData = from parseResult in data
            group parseResult by parseResult.IPAddress into resultGroup
            orderby resultGroup.Count() descending
               select new LogAnalyzerGroupData
               {
                    Data = resultGroup.Key,
                    Count = resultGroup.Count(),
               };
            return GetOutputData(groupedData,numberOfActiveIP);
            
        }
        private IReadOnlyCollection<string> GetOutputData(IEnumerable<LogAnalyzerGroupData> data, int count )
        {
            List<string> outData = new List<string>();
            int outCount = 0;
            foreach (var result in data)
            {
                if (outCount < count)
                {
                    outData.Add(result.Data);
                }
                else
                {
                    break;
                }
                outCount++;
            }
            return outData.AsReadOnly();
        }

        public IReadOnlyCollection<string> GetMostVisitedUrl(ICollection<LogParserResult> data, int mostVisitedURLCount=3)
        {
            // Not considering the HttpMethod type since it is most visited url
            // A get and post would be considered as the same url 
            var groupedData = from parseResult in data
                              group parseResult by parseResult.Url into resultGroup
                              orderby resultGroup.Count() descending
                              select new LogAnalyzerGroupData
                              {
                                  Data = resultGroup.Key,
                                  Count = resultGroup.Count(),
                              };
            return GetOutputData(groupedData, mostVisitedURLCount);
        }

        public int GetUniqueIpAddressCount(ICollection<LogParserResult> data)
        {
            return data.Select(s => s.IPAddress).Distinct().Count();
        }
    }
}
