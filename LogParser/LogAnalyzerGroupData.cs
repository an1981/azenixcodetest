﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser
{
    public class LogAnalyzerGroupData
    {
        public int Count;
        public string Data { get; set; }
    }
}
