﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser
{
    public interface ILogAnalyzer
    {
        int GetUniqueIpAddressCount(ICollection<LogParserResult> data);
        IReadOnlyCollection<string> GetMostActiveIP(ICollection<LogParserResult> data,int numberOfActiveIP=3);
        IReadOnlyCollection<string> GetMostVisitedUrl(ICollection<LogParserResult> data, int mostVisitedURLCount=3);

    }
}
