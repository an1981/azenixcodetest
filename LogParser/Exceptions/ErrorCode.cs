﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Exceptions
{
    public static class ErrorCode
    {
        public static readonly string FILE_NOT_FOUND = "File_Not_Found";
        public static readonly string URL_NOT_FOUND = "Url_Not_Found";
        public static readonly string IP_NOT_FOUND = "IP_Not_Found";
    }
}
