﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Exceptions
{
    public class FileNotFoundException : LogParserException
    {
        public FileNotFoundException(string fileName) : base($"Input file {fileName} not found")
        {
            Code = ErrorCode.FILE_NOT_FOUND;
        }
    }
}
