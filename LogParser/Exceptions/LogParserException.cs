﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Exceptions
{
    public class LogParserException : Exception
    {
        public LogParserException(string message)
           : base(message)
        {

        }
        public string Code { get; protected set; }
    }
}
