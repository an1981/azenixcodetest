﻿using System;
using System.Text.RegularExpressions;
using System.IO;
using LogParser.Exceptions;
using System.Collections.Generic;

namespace LogParser
{
    public class LogParser : ILogParser
    {
        private readonly Regex ipRegex;
        private readonly Regex urlRegex;
        public LogParser()
        {
            string ipPattern = @"(?<ip>\S+)";
            string urlPattern = "(?<url>\"(.*?)\")";

            ipRegex = new Regex(ipPattern);
            urlRegex = new Regex(urlPattern);
        }
        private string GetUrl(string quotedText)
        {
            if (!string.IsNullOrEmpty(quotedText))
            {
                var array = quotedText.Split(' ');
                if (array.Length >= 2)
                {
                    return array[1];
                }
            }
            return string.Empty;
        }
        public IReadOnlyCollection<LogParserResult> ParseLogFromFile(string fileName)
        {
            List<LogParserResult> outputData = new List<LogParserResult>();
            if (!File.Exists(fileName))
            {
                throw new Exceptions.FileNotFoundException(fileName);
            }
            string[] fileLines = File.ReadAllLines(fileName);
            Match match = null;
            foreach (var fileLine in fileLines)
            {
                // Code design smell if things have to be changed 
                // if a new field needs to be parsed
                var logParserResult = new LogParserResult();

                match = ipRegex.Match(fileLine);
                if (match.Success)
                {
                    logParserResult.IPAddress = match.Groups["ip"].ToString();
                }
                else
                {
                    //log error 
                    
                }
                match = urlRegex.Match(fileLine);
                if (match.Success)
                {
                    logParserResult.Url = GetUrl(match.Groups["url"].ToString());

                }
                else
                {
                    //log error 
                }
                outputData.Add(logParserResult);
            }
            return outputData.AsReadOnly();
        }

        
    }
}
