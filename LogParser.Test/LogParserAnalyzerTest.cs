using System;
using Xunit;
using FluentAssertions;
using System.Collections.Generic;

namespace LogParser.Test
{
    public class LogParserAnalyzerTest
    {
        private readonly LogAnalyzer logAnalyzer;
        public LogParserAnalyzerTest()
        {
            logAnalyzer = new LogAnalyzer();
        }
        [Fact]
        public void ShouldGetTheTotalUniqueIPs()
        {
            var data = Helpers.GetLogData();
            logAnalyzer.GetUniqueIpAddressCount(data).Should().Be(3);
        }
        [Fact]
        public void ShouldGetTheMostVisitedIPs()
        {
            var data = Helpers.GetLogData();
            List<string> mostVisitedIPList = new List<string>(logAnalyzer.GetMostActiveIP(data));
            mostVisitedIPList.Count.Should().Be(3);
            mostVisitedIPList[0].Should().Be("10.0.0.0");
            mostVisitedIPList[1].Should().Be("20.0.0.0");
            mostVisitedIPList[2].Should().Be("30.0.0.0");


            mostVisitedIPList = new List<string>(logAnalyzer.GetMostActiveIP(data,2));
            mostVisitedIPList.Count.Should().Be(2);
            mostVisitedIPList[0].Should().Be("10.0.0.0");
            mostVisitedIPList[1].Should().Be("20.0.0.0");
             
        }

        [Fact]
        public void ShouldGetMostActiveUrl()
        {
            var data = Helpers.GetLogData();
            List<string> mostVisitedUrlList = new List<string>(logAnalyzer.GetMostVisitedUrl(data));
            mostVisitedUrlList.Count.Should().Be(3);
            mostVisitedUrlList[0].Should().Be("http://url1");
            mostVisitedUrlList[1].Should().Be("http://url5");
            mostVisitedUrlList[2].Should().Be("http://url4");

            mostVisitedUrlList = new List<string>(logAnalyzer.GetMostVisitedUrl(data,2));
            mostVisitedUrlList.Count.Should().Be(2);
            mostVisitedUrlList[0].Should().Be("http://url1");
            mostVisitedUrlList[1].Should().Be("http://url5");
        }
    }
}
