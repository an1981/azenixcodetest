﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentValidation;
using FluentAssertions;
using System.Linq;

namespace LogParser.Test
{
    public class LogParserTest
    {

        private readonly LogParser logParser;
        public LogParserTest()
        {
            logParser = new LogParser();
        }

        [Fact]
        public void ShouldParseTheLogs()
        {
            var parsedData = logParser.ParseLogFromFile("sampledata.txt");
            parsedData.Count.Should().Be(6);
            List<LogParserResult> data = new List<LogParserResult>(parsedData);
            data.Select(s => s.IPAddress).Contains("177.71.128.21").Should().BeTrue();
            data.Select(s => s.Url).Contains("http://example.net/faq/").Should().BeTrue();

        }
    }
}
