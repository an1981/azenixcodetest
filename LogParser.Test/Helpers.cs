﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Test
{
    public static class Helpers
    {
        public static IList<LogParserResult> GetLogData()
        {
            List<LogParserResult> logParserResults = new List<LogParserResult>();
            logParserResults.Add(new LogParserResult()
            {
                IPAddress = "10.0.0.0",
                Url = "http://url1"
            });
            logParserResults.Add(new LogParserResult()
            {
                IPAddress = "10.0.0.0",
                Url = "http://url5"
            });
            logParserResults.Add(new LogParserResult()
            {
                IPAddress = "10.0.0.0",
                Url = "http://url1"
            });
            logParserResults.Add(new LogParserResult()
            {
                IPAddress = "20.0.0.0",
                Url = "http://url1"
            });
            logParserResults.Add(new LogParserResult()
            {
                IPAddress = "20.0.0.0",
                Url = "http://url4"
            });
            logParserResults.Add(new LogParserResult()
            {
                IPAddress = "30.0.0.0",
                Url = "http://url5"
            });
            return logParserResults;
        }
    }
}
