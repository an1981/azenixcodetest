﻿using LogParser.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
 
using System;
using System.Threading.Tasks;

namespace LogParser.Caller
{
    public class App
    {
        private readonly ILogger<App> _logger;
        private readonly IConfigurationRoot _configuration;
        private readonly ILogProcessor _logProcessor;
        public App(ILogger<App> logger,IConfigurationRoot configuration,ILogProcessor logProcessor)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configuration = configuration;
            _logProcessor = logProcessor;
        }

        public async Task Run(string[] args)
        {
            _logger.LogInformation("Starting...");


            LogProcessorResult data = _logProcessor.ProcessLogsFromFile(_configuration["FileName"]);
            Console.WriteLine($"Total numnber Unique IP address: {data.UniqueIPCount}");
            
                 
            Console.WriteLine("Most active IP ");
            foreach(var ipaddress in data.MostActiveIPAddress)
            {
                Console.WriteLine(ipaddress);
            }

            Console.WriteLine("Most Visited URL ");
            foreach (var url in data.MostVisitedUrl)
            {
                Console.WriteLine(url);
            }

            _logger.LogInformation("Finished!");

            await Task.CompletedTask;
        }
    }
}
