﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;


namespace LogParser.Caller
{
    class Program
    {
        private static IServiceProvider _serviceProvider;
        
        public static async Task Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            // create service provider
            _serviceProvider = services.BuildServiceProvider();

            // entry to run app
            await _serviceProvider.GetService<App>().Run(args);
            DisposeServices();

        }
        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
        private static void ConfigureServices(IServiceCollection services)
        {
            // configure logging
            services.AddLogging(builder =>
            {
                builder.AddConsole();
                builder.AddDebug();
            });

            // build config
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false)
                .AddEnvironmentVariables()
                .Build();

            services.AddSingleton<IConfigurationRoot>(configuration);

            // add services:
            services.AddTransient<ILogParser, LogParser>();
            services.AddTransient<ILogAnalyzer, LogAnalyzer>();
            services.AddTransient<ILogProcessor, LogProcessor>();
            // add app
            services.AddTransient<App>();
        }
    }
}
